﻿using ServicioVeterinaria.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Services.Service
{
    public interface IProviderService
    {
        IEnumerable<Provider> GetProviders();
        Provider GetUser(int id);
        Provider Add(Provider Provider);
        bool Update(int id, Provider Provider);
        bool Delete(int id);
    }
    public class ProviderService : IProviderService
    {
        public Provider Add(Provider Provider)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Provider> GetProviders()
        {
            throw new NotImplementedException();
        }

        public Provider GetUser(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(int id, Provider Provider)
        {
            throw new NotImplementedException();
        }
    }
}
