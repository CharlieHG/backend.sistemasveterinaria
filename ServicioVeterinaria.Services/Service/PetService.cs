﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Services.Service
{
    public interface IPetService
    {
        IEnumerable<Pet> GetPets();
        Pet GetPet(Guid id);
        Pet Add(Pet Pet);
        bool Update(Guid id, Pet Pet);
        bool Delete(Guid id);
    }
    public class PetService : IPetService
    {
        private readonly IPetRepository _petRepository;
        public PetService(IPetRepository petRepository)
        {
            _petRepository = petRepository;
        }
        public Pet Add(Pet Pet)
        {
            return _petRepository.Add(Pet);
        }

        public bool Delete(Guid id)
        {
            if (_petRepository.GetPet(id) != null)
                return _petRepository.Delete(id);
            else return false;
        }

        public Pet GetPet(Guid id)
        {
            return _petRepository.GetPet(id);
        }

        public IEnumerable<Pet> GetPets()
        {
            return _petRepository.GetPets();
        }

        public bool Update(Guid id, Pet Pet)
        {
            if (_petRepository.GetPet(id) != null)
                return _petRepository.Update(id, Pet);
            else return false;
        }
    }
}
