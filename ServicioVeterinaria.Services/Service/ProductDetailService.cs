﻿using ServicioVeterinaria.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Services.Service
{
    public interface IProductDetailService
    {
        IEnumerable<ProductDetail> GetSale();
        ProductDetail GetUser(int id);
        ProductDetail Add(ProductDetail Sale);
        bool Update(int id, ProductDetail Sale);
        bool Delete(int id);

    }
    public class ProductDetailService : IProductDetailService
    {
        public ProductDetail Add(ProductDetail Sale)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductDetail> GetSale()
        {
            throw new NotImplementedException();
        }

        public ProductDetail GetUser(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(int id, ProductDetail Sale)
        {
            throw new NotImplementedException();
        }
    }
}
