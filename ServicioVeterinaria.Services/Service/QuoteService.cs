﻿using ServicioVeterinaria.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Services.Service
{
    public interface IQuoteService
    {
        IEnumerable<Quote> GetQuotes();
        Quote GetQuote(int id);
        Quote Add(Quote Quote);
        bool Update(int id, Quote Quote);
        bool Delete(int id);
    }
    public class QuoteService : IQuoteService
    {
        public Quote Add(Quote Quote)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Quote GetQuote(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Quote> GetQuotes()
        {
            throw new NotImplementedException();
        }

        public bool Update(int id, Quote Quote)
        {
            throw new NotImplementedException();
        }
    }
}
