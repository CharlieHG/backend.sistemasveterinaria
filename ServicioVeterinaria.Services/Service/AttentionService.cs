﻿using System;
using System.Collections.Generic;
using ServicioVeterinaria.Model.Models;
using System.Text;
using ServicioVeterinaria.Repository;

namespace ServicioVeterinaria.Services.Service
{
    public interface IAttentionService
    {
        IEnumerable<Attention> GetAttentions();

        Attention GetAttention(int id);
        Attention Add(Attention attention);
        bool Update(int id, Attention attention);
        bool Delete(int id);
    }
    public class AttentionService : IAttentionService
    {
        private readonly IAttentionRepository _attentionRepository;
        public AttentionService(IAttentionRepository attentionRepository)
        {
            _attentionRepository = attentionRepository;
        }
        public Attention Add(Attention attention)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Attention GetAttention(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Attention> GetAttentions()
        {
            return _attentionRepository.GetAttentions();
        }

        public bool Update(int id, Attention attention)
        {
            throw new NotImplementedException();
        }
    }
}
