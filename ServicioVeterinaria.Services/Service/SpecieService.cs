﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Services.Service
{
    public interface ISpecieService
    {
        IEnumerable<Specie> GetSpecies();
        Specie GetSpecie(Guid id);
        Specie Add(Specie specie);
        bool Update(Guid id, Specie specie);
    }
    public class SpecieService : ISpecieService
    {
        private readonly ISpecieRepository _specieRepository;
        public SpecieService(ISpecieRepository specieRepository)
        {
            _specieRepository = specieRepository;
        }
        public Specie Add(Specie specie)
        {
            return _specieRepository.Add(specie);
        }

        public IEnumerable<Specie> GetSpecies()
        {
            return _specieRepository.GetSpecies();
        }

        public Specie GetSpecie(Guid id)
        {
            return _specieRepository.GetSpecie(id);
        }

        public bool Update(Guid id, Specie specie)
        {
            if (_specieRepository.GetSpecie(id) != null)
                return _specieRepository.Update(specie);
            else return false;
        }
    }
}
