﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Services.Service
{
    public interface IClientService
    {
        IEnumerable<Client> GetClients();
        Client GetClient(Guid id);
        Client Add(Client Client);
        bool Update(Guid id, Client Client);
        bool Delete(Guid id);

    }
    public class ClientService : IClientService
    {
        private readonly IClientRepository _ClientRepository;
        public ClientService(IClientRepository ClientRepository)
        {
            _ClientRepository = ClientRepository;
        }
        public Client Add(Client Client)
        {
            return _ClientRepository.Add(Client);
        }

        public bool Delete(Guid id)
        {
            if (_ClientRepository.GetClient(id) != null)
                return _ClientRepository.Delete(id);
            else return false;
        }

        public IEnumerable<Client> GetClients()
        {
            return _ClientRepository.GetClients();
        }

        public Client GetClient(Guid id)
        {
            return _ClientRepository.GetClient(id);
        }

        public bool Update(Guid id, Client Client)
        {
            if (_ClientRepository.GetClient(id) != null)
                return _ClientRepository.Update(id, Client);
            else return false;
        }
    }
}
