﻿using ServicioVeterinaria.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Services.Service
{
    public interface IAttentionDetailService
    {
        IEnumerable<AttentionDetail> GetAttentionDetails();
        AttentionDetail GetServiceDetail(int id);
        AttentionDetail Add(AttentionDetail attentionDetail);
        bool Update(int id, AttentionDetail attentionDetail);
        bool Delete(int id);
    }
    public class AttentionDetailService : IAttentionDetailService
    {
        public AttentionDetail Add(AttentionDetail attentionDetail)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AttentionDetail> GetAttentionDetails()
        {
            throw new NotImplementedException();
        }

        public AttentionDetail GetServiceDetail(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(int id, AttentionDetail attentionDetail)
        {
            throw new NotImplementedException();
        }
    }
}
