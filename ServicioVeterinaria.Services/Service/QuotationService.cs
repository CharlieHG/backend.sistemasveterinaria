﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Services.Service
{
    public interface IQuotationService
    {
        IEnumerable<Quotation> GetQuotations();
        Quotation GetQuotation(int id);
        Quotation Add(Quotation Quotation);
        bool Update(int id, Quotation Quotation);
        bool Delete(int id);
    }
    public class QuotationService : IQuotationService
    {
        private readonly IQuotationRepository _QuotationRepository;
        private readonly IProductRepository _productRepository;
        public QuotationService(IQuotationRepository QuotationRepository, IProductRepository productRepository)
        {
            _QuotationRepository = QuotationRepository;
            _productRepository = productRepository;
        }

        public Quotation Add(Quotation Quotation)
        {

            foreach (var item in Quotation.ProductDetail)
            {
                var quantity = _productRepository.GetProduct(item.ProductId);
                quantity.Stock = quantity.Stock - item.Quantity;
                _productRepository.Update(item.Id, quantity);
            }


            _QuotationRepository.Add(Quotation);
            return Quotation;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Quotation GetQuotation(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Quotation> GetQuotations()
        {
            return _QuotationRepository.GetQuotations();
        }

        public bool Update(int id, Quotation Quotation)
        {
            throw new NotImplementedException();
        }
    }
}
