﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Services.Service
{
    public interface IProductService
    {
        IEnumerable<Product> GetProducts();
        IEnumerable<Product> GetProductsFiltered();
        Product GetUser(int id);
        Product Add(Product Product);
        bool Update(Guid id, Product Product);
        bool Delete(Guid id);
    }
    public class ProductService : IProductService
    {
        private readonly IProductRepository _ProductRepository;

        public ProductService(IProductRepository productRepository)
        {
            _ProductRepository = productRepository;
        }
        public Product Add(Product Product)
        {
            return _ProductRepository.Add(Product);
        }

        public bool Delete(Guid id)
        {
            if (_ProductRepository.GetProduct(id) != null)
                return _ProductRepository.Delete(id);
            else return false;
        }


        public Product GetProduct(Guid id)
        {
            return _ProductRepository.GetProduct(id);
        }

        public IEnumerable<Product> GetProducts()
        {
            return _ProductRepository.GetProducts();
        }

        public Product GetUser(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(int id, Product Product)
        {
            throw new NotImplementedException();
        }
    }
}
