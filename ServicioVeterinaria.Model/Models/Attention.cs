﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class Attention : Entity
    {
        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres mínimo")]
        public string Name { get; set; }

        [Required]
        public virtual Guid EmployeeId { get; set; }

        [Required]
        public double Price { get; set; }
    }
}
