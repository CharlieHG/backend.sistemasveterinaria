﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class ProductDetail : Entity
    {
        [Required]
        public virtual Guid ProductId { get; set; }

        [Required]
        public int Quantity { get; set; }

        //El Subtotal se obtiene multiplicando el precio del Product por la Quantity
        [Required]
        public double Subtotal { get; set; }

        [Required]
        public Guid QuotationId { get; set; }
    }
}
