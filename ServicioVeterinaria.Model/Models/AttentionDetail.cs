﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class AttentionDetail : Entity
    {
        [Required]

        public Guid AttentionId { get; set; }

        [Required]
        public Guid QuotationId { get; set; }
    }
}
