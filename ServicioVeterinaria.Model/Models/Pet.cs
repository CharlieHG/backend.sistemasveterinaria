﻿using ServicioVeterinaria.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class Pet : Entity
    {
        [Required(ErrorMessage = "Campo Obligatorio")]
        public string Name { get; set; }

        [Required (ErrorMessage = "Campo Obligatorio")]
        public virtual Specie Specie { get; set; }

     
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        public GenderEnum Gender { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        public double Weight { get; set; }

        [Required]
        public Guid ClientId { get; set; }
    }
}
