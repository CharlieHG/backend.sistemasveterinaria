﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class Specie
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres mínimo")]
        public string Name { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres mínimo")]
        public string Breed { get; set; }
    }
}
