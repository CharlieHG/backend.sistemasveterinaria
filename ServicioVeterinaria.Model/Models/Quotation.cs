﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    //La Quotation debe llevar una o mas Sales y/o un ServiceDetail
    public class Quotation : Entity
    {
        //Lleva el cliente únicamente si lleva un ServiceDetail
        public  Guid ClientId { get; set; }
        //Lleva la mascota del cliente si lleva el Client
        public Guid PetId { get; set; }

        //Puede o no llevar un ServiceDetail
        //public virtual ServiceDetail ServiceDetail { get; set; }
        public virtual IEnumerable<AttentionDetail> AttentionDetail { get; set; }


        //Puede o no llevar una o mas Sales
        public virtual IEnumerable<ProductDetail> ProductDetail { get; set; }


        //El Total se calcula sumando los subtotales del ServiceDetail y las Sales
        //[Required]
        public double Total { get; set; }
    }
}
