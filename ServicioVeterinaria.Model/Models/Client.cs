﻿using ServicioVeterinaria.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class Client : Entity
    {
        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres mínimo")]
        public string Name { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres mínimo")]
        public string LastName { get; set; }

        [Required]
        public GenderEnum Gender { get; set; }

        [Required]
        [MaxLength(100, ErrorMessage = "100 caracteres máximo")]
        [EmailAddress(ErrorMessage = "Correo electrónico inválido")]
        public string Email { get; set; }

        [Required]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Número de teléfono inválido")]
        public string PhoneNumber { get; set; }

        //Debe tener mínimo una Pet
        public virtual IEnumerable<Pet> Pets { get; set; }
    }
}
