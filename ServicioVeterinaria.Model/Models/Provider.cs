﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class Provider : Entity
    {
        [Required]
        [MaxLength(100, ErrorMessage = "100 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres mínimo")]
        public string BusinessName { get; set; }

        [Required]
        [MaxLength(15, ErrorMessage = "15 caracteres máximo")]
        [MinLength(7, ErrorMessage = "7 numeros minimo")]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{7,15}$", ErrorMessage = "Número de teléfono invalido")]
        public string PhoneNumber { get; set; }

        [Required]
        [MaxLength(200, ErrorMessage = "200 caracteres máximo")]
        [MinLength(1, ErrorMessage = "1 carácter mínimo")]
        public string Address { get; set; }

        [Required]
        public virtual IEnumerable<Product> Products { get; set; }
    }
}
