﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class Entity
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public bool IsActive { get; set; }
    }
}
