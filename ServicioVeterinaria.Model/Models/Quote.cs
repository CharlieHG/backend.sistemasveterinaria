﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class Quote: Entity
    {
        [Required]
        public Guid IdClient { get; set; }

        [Required]
        public Guid IdUser { get; set; }

        [Required]
        public Guid IdPet { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public virtual Attention Service { get; set; }
    }
}
