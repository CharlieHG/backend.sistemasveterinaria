﻿using ServicioVeterinaria.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class User : Entity
    {
        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres mínimo")]
        public string Name { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres mínimo")]
        public string LastName { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(5, ErrorMessage = "5 caracteres mínimo")]
        [RegularExpression("^[a-zA-Z0-9_.-]*$", ErrorMessage = "Solo letras y números, no caracteres especiales")]
        public string Username { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(5, ErrorMessage = "5 caracteres mínimo")]
        public string Password { get; set; }

        [Required]
        public GenderEnum Gender { get; set; }

        [Required]
        public RoleEnum Role { get; set; }
    }
}
