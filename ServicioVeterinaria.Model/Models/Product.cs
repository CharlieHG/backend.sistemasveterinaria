﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServicioVeterinaria.Model.Models
{
    public class Product : Entity
    {
        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres mínimo")]
        public string Name { get; set; }

        [Required]
        [MaxLength(200, ErrorMessage = "200 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres minimo")]
        public string Description { get; set; }

        [Required]
        public double Price { get; set; }

        [Required]
        public int Stock { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "50 caracteres máximo")]
        [MinLength(2, ErrorMessage = "2 caracteres mínimo")]
        public string Category { get; set; }

        //Lleva fecha de vencimiento si el producto es un perecedero
        public DateTime? DateExpiry { get; set; }

        [Required]
        public Guid ProviderId { get; set; }
    }
}
