﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Model.Enums
{
    public enum RoleEnum
    {
        Administrator = 1,
        User = 2
    }
}
