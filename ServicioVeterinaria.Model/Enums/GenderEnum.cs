﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Model.Enums
{
    public enum GenderEnum
    {
        Male = 1,
        Female = 2
    }
}
