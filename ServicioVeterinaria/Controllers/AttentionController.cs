﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicioVeterinaria.Services.Service;

namespace ServicioVeterinaria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttentionController : ControllerBase
    {
        private readonly IAttentionService _attentionService;

        public AttentionController(IAttentionService attentionService)
        {
            _attentionService = attentionService;
        }
        // GET: api/Service
        [HttpGet]
        public ActionResult GetServices()
        {
            return Ok(_attentionService.GetAttentions());
        }

        // GET: api/Service/5
        [HttpGet("{id}")]
        public string GetService(int id)
        {
            return "value";
        }

        // POST: api/Service
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Service/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
