﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ServicioVeterinaria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttentionDetailController : ControllerBase
    {
        // GET: api/ServiceDetail
        [HttpGet]
        public IEnumerable<string> GetServiceDetails()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ServiceDetail/5
        [HttpGet("{id}")]
        public string GetServiceDetail(int id)
        {
            return "value";
        }

        // POST: api/ServiceDetail
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/ServiceDetail/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
