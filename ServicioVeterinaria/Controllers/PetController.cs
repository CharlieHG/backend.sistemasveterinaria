﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Services.Service;

namespace ServicioVeterinaria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PetController : ControllerBase
    {
        private readonly IPetService _petService;
        public PetController(IPetService petService)
        {
            _petService = petService;
        }
        // GET: api/Pet
        [HttpGet]
        public ActionResult GetPets()
        {
            return Ok(_petService.GetPets());
        }

        // GET: api/Pet/5
        [HttpGet("{id}")]
        public ActionResult GetPet(Guid id)
        {
            try
            {
                return Ok(_petService.GetPet(id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // POST: api/Pet
        [HttpPost]
        public ActionResult Post([FromBody] Pet pet)
        {
            try
            {
                if (ModelState.IsValid)
                    return Ok(_petService.Add(pet));
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // PUT: api/Pet/5
        [HttpPut("{id}")]
        public ActionResult Put(Guid id, [FromBody] Pet pet)
        {
            try
            {
                if (_petService.Update(id, pet))
                    return Ok();
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (_petService.Delete(id))
                    return Ok();
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
