﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ServicioVeterinaria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuoteController : ControllerBase
    {
        // GET: api/Quote
        [HttpGet]
        public IEnumerable<string> GetQuotes()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Quote/5
        [HttpGet("{id}")]
        public string GetQuote(int id)
        {
            return "value";
        }

        // POST: api/Quote
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Quote/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
