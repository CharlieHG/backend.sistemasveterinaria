﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Services.Service;

namespace ServicioVeterinaria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuotationController : ControllerBase
    {
        private readonly IQuotationService _quotationService;

        public QuotationController(IQuotationService quotationService)
        {
            _quotationService = quotationService;
        }
        // GET: api/Quotation
        [HttpGet]
        public ActionResult GetQuotations()
        {
            return Ok(_quotationService.GetQuotations());
        }

        // GET: api/Quotation/5
        [HttpGet("{id}")]
        public string GetQuotation(int id)
        {
            return "value";
        }

        // POST: api/Quotation
        [HttpPost]
        public ActionResult Post([FromBody]Quotation quotation)
        {

            return Ok(_quotationService.Add(quotation));
        }

        // PUT: api/Quotation/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
