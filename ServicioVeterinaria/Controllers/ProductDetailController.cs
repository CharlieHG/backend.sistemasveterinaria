﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ServicioVeterinaria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductDetailController : ControllerBase
    {
        // GET: api/Sale
        [HttpGet]
        public IEnumerable<string> GetSales()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Sale/5
        [HttpGet("{id}")]
        public string GetSale(int id)
        {
            return "value";
        }

        // POST: api/Sale
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Sale/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
