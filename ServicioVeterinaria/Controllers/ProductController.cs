﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Services.Service;

namespace ServicioVeterinaria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        // GET: api/Product
        [HttpGet]
        public ActionResult GetProducts()
        {
            var products = _productService.GetProducts();
            return Ok(products);
        }

        [HttpGet("filter")]
        public ActionResult GetProductsFiltered()
        {
            var products = _productService.GetProductsFiltered();
            return Ok(products);
        }

        // GET: api/Product/5
        [HttpGet("{id}")]
        public ActionResult GetProduct(Guid id)
        {
            try
            {
                return Ok(_productService.GetProduct(id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // POST: api/Product
        [HttpPost]
        public ActionResult Post([FromBody] Product product)
        {
            try
            {
                if (ModelState.IsValid)
                    return Ok(_productService.Add(product));
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        public ActionResult Put(Guid id, [FromBody] Product product)
        {
            try
            {
                if (_productService.Update(id, product))
                    return Ok();
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (_productService.Delete(id))
                    return Ok();
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
