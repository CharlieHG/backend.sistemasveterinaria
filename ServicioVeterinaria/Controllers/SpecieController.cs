﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ServicioVeterinaria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpecieController : ControllerBase
    {
        // GET: api/Specie
        [HttpGet]
        public IEnumerable<string> GetSpecies()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Specie/5
        [HttpGet("{id}")]
        public string GetSpecie(int id)
        {
            return "value";
        }

        // POST: api/Specie
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Specie/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
