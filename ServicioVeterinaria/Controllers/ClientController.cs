﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Services.Service;

namespace ServicioVeterinaria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly IClientService _clientService;
        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }
        // GET: api/Client
        [HttpGet]
        public ActionResult GetClients()
        {
            return Ok(_clientService.GetClients());
        }

        // GET: api/Client/5
        [HttpGet("{id}")]
        public ActionResult GetClient(Guid id)
        {
            try
            {
                return Ok(_clientService.GetClient(id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // POST: api/Client
        [HttpPost]
        public ActionResult Post([FromBody] Client client)
        {
            try
            {
                if (ModelState.IsValid)
                    return Ok(_clientService.Add(client));
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // PUT: api/Client/5
        [HttpPut("{id}")]
        public ActionResult Put(Guid id, [FromBody] Client client)
        {
            try
            {
                if (_clientService.Update(id, client))
                    return Ok();
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (_clientService.Delete(id))
                    return Ok();
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
