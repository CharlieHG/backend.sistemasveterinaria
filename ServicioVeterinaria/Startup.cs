﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ServicioVeterinaria.Repository;
using ServicioVeterinaria.Services.Service;

namespace ServicioVeterinaria
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IClientRepository, ClientRepository>();
            services.AddTransient<IPetRepository, PetRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IProviderRepository, ProviderRepository>();
            services.AddTransient<IQuotationRepository, QuotationRepository>();
            services.AddTransient<IQuoteRepository, QuoteRepository>();
            services.AddTransient<IProductDetailRepository, ProductDetailRepository>();
            services.AddTransient<IAttentionDetailRepository, AttentionDetailRepository>();
            services.AddTransient<IAttentionRepository, AttentionRepository>();
            services.AddTransient<ISpecieRepository, SpecieRepository>();

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IClientService, ClientService>();
            services.AddTransient<IPetService, PetService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IProviderService, ProviderService>();
            services.AddTransient<IQuotationService, QuotationService>();
            services.AddTransient<IQuoteService, QuoteService>();
            services.AddTransient<IAttentionDetailService, AttentionDetailService>();
            services.AddTransient<IAttentionService, AttentionService>();
            services.AddTransient<ISpecieService, SpecieService>();
            services.AddTransient<IProductDetailService, ProductDetailService>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(options => options.WithOrigins("http://localhost:4200/").AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin().AllowCredentials());
            app.UseMvc();
        }
    }
}
