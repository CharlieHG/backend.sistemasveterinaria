﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Repository
{
    public interface IQuotationRepository
    {
        IEnumerable<Quotation> GetQuotations();
        Quotation GetQuotation(int id);
        Quotation Add(Quotation Quotation);
        bool Update(int id, Quotation Quotation);
        bool Delete(int id);
    }
    public class QuotationRepository : IQuotationRepository
    {
        private readonly DataDbContext _context = new DataDbContext();

        public Quotation Add(Quotation Quotation)
        {
             _context.Quotation.Add(Quotation);
            _context.SaveChanges();
            return Quotation;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Quotation GetQuotation(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Quotation> GetQuotations()
        {
            return _context.Set<Quotation>();
        }

        public bool Update(int id, Quotation Quotation)
        {
            throw new NotImplementedException();
        }
    }
}
