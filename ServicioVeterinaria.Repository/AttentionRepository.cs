﻿using System;
using System.Collections.Generic;
using ServicioVeterinaria.Model.Models;
using System.Text;
using ServicioVeterinaria.Repository.Data;

namespace ServicioVeterinaria.Repository
{
    public interface IAttentionRepository
    {
        IEnumerable<Attention> GetAttentions();
        Attention GetAttention(int id);
        Attention Add(Attention attention);
        bool Update(int id, Attention attention);
        bool Delete(int id);
    }
    public class AttentionRepository : IAttentionRepository
    {
        private readonly DataDbContext _context = new DataDbContext();

        public Attention Add(Attention attention)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Attention GetAttention(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Attention> GetAttentions()
        {
            return _context.Set<Attention>();
        }

        public bool Update(int id, Attention attention)
        {
            throw new NotImplementedException();
        }
    }
}
