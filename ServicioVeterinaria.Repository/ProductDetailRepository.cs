﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Repository
{
    public interface IProductDetailRepository
    {
        IEnumerable<ProductDetail> GetSale();
        ProductDetail GetUser(int id);
        ProductDetail Add(ProductDetail Sale);
        bool Update(int id, ProductDetail Sale);
        bool Delete(int id);

    }
    public class ProductDetailRepository : IProductDetailRepository
    {
        private readonly DataDbContext _context = new DataDbContext();

        public ProductDetail Add(ProductDetail Sale)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductDetail> GetSale()
        {
            return _context.Set<ProductDetail>();
        }

        public ProductDetail GetUser(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(int id, ProductDetail Sale)
        {
            throw new NotImplementedException();
        }
    }
}
