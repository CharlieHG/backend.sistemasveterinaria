﻿using Microsoft.EntityFrameworkCore;
using ServicioVeterinaria.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Repository.Data
{
    public class DataDbContext : DbContext
    {
        public DataDbContext()
        {

        }
        public DataDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer(@"Server=DESKTOP-FVPME4Q\UTS;Database=Veterinary;Uid=sa;Pwd=uts;");
                //optionsBuilder.UseSqlServer(@"Server=.;Database=Veterinary;Uid=sa;Pwd=uts;");
            }

            optionsBuilder
     .UseLazyLoadingProxies();
        }

        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Pet> Pet { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Provider> Provider { get; set; }
        public virtual DbSet<Quotation> Quotation { get; set; }
        public virtual DbSet<ProductDetail> ProductDetail { get; set; }
        public virtual DbSet<Attention> Attention { get; set; }
        public virtual DbSet<AttentionDetail> AttentionDetail { get; set; }
        public virtual DbSet<Specie> Specie { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Quote> Quote { get; set; }
    }
}
