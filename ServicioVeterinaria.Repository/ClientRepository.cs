﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServicioVeterinaria.Repository
{
    public interface IClientRepository
    {
        IEnumerable<Client> GetClients();
        Client GetClient(Guid id);
        Client Add(Client Client);
        bool Update(Guid id, Client newClient);
        bool Delete(Guid id);

    }
    public class ClientRepository : IClientRepository
    {
        private readonly DataDbContext _context = new DataDbContext();
        public Client Add(Client Client)
        {
            _context.Add(Client);
            _context.SaveChanges();
            return Client;
        }

        public bool Delete(Guid id)
        {
            var client = GetClient(id);
            client.IsActive = !client.IsActive;
            _context.Client.Update(client);
            _context.SaveChanges();
            return true;
        }

        public IEnumerable<Client> GetClients()
        {
          return _context.Set<Client>();
        }

        public Client GetClient(Guid id)
        {
            return _context.Client.Where(c => c.Id == id).FirstOrDefault();
        }

        public bool Update(Guid id, Client newClient)
        {
            var client = GetClient(id);
            client.Email = newClient.Email;
            client.Gender = newClient.Gender;
            client.LastName = newClient.LastName;
            client.Name = newClient.Name;
            client.PhoneNumber = client.PhoneNumber;
            _context.Client.Update(client);
            _context.SaveChanges();
            return true;
        }
    }
}
