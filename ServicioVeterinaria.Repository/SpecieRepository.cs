﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServicioVeterinaria.Repository
{
    public interface ISpecieRepository
    {
        IEnumerable<Specie> GetSpecies();
        Specie GetSpecie(Guid id);
        Specie Add(Specie specie);
        bool Update(Specie newSpecie);
    }
    public class SpecieRepository : ISpecieRepository
    {
        private readonly DataDbContext _context = new DataDbContext();
        public Specie Add(Specie specie)
        {
            _context.Add(specie);
            _context.SaveChanges();
            return specie;
        }

        public IEnumerable<Specie> GetSpecies()
        {
            return _context.Set<Specie>();
        }

        public Specie GetSpecie(Guid id)
        {
            return _context.Specie.Where(s => s.Id == id).FirstOrDefault();
        }

        public bool Update(Specie newSpecie)
        {
            var specie = GetSpecie(newSpecie.Id);
            specie.Name = newSpecie.Name;
            specie.Breed = newSpecie.Breed;
            _context.Specie.Update(specie);
            _context.SaveChanges();
            return true;
        }
    }
}
