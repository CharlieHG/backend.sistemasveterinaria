﻿using ServicioVeterinaria.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Repository
{
    public interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        User GetUser(int id);
        User Add(User user);
        bool Update(int id, User user);
        bool Delete(int id);
    }
    public class UserRepository : IUserRepository
    {
        public User Add(User user)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public User GetUser(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetUsers()
        {
            throw new NotImplementedException();
        }

        public bool Update(int id, User user)
        {
            throw new NotImplementedException();
        }
    }
}
