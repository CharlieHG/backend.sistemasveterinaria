﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class init9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Quotation_ClientId",
                table: "Quotation",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Quotation_Client_ClientId",
                table: "Quotation",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Quotation_Client_ClientId",
                table: "Quotation");

            migrationBuilder.DropIndex(
                name: "IX_Quotation_ClientId",
                table: "Quotation");
        }
    }
}
