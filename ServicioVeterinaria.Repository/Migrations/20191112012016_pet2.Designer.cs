﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ServicioVeterinaria.Repository.Data;

namespace ServicioVeterinaria.Repository.Migrations
{
    [DbContext(typeof(DataDbContext))]
    [Migration("20191112012016_pet2")]
    partial class pet2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Attention", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("EmployeeId");

                    b.Property<bool>("IsActive");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<double>("Price");

                    b.HasKey("Id");

                    b.HasIndex("EmployeeId");

                    b.ToTable("Attention");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.AttentionDetail", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("IdQuotation");

                    b.Property<bool>("IsActive");

                    b.Property<Guid?>("QuotationId");

                    b.Property<Guid>("ServicesId");

                    b.HasKey("Id");

                    b.HasIndex("QuotationId");

                    b.HasIndex("ServicesId");

                    b.ToTable("AttentionDetail");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Client", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("Gender");

                    b.Property<bool>("IsActive");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("PhoneNumber")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Client");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Pet", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("BirthDate");

                    b.Property<Guid>("ClientId");

                    b.Property<int>("Gender");

                    b.Property<bool>("IsActive");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<double>("Weight");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.HasIndex("Name");

                    b.ToTable("Pet");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Product", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Category")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateExpiry");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<bool>("IsActive");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<double>("Price");

                    b.Property<Guid>("ProviderId");

                    b.Property<int>("Stock");

                    b.HasKey("Id");

                    b.HasIndex("ProviderId");

                    b.ToTable("Product");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.ProductDetail", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsActive");

                    b.Property<Guid>("ProductId");

                    b.Property<int>("Quantity");

                    b.Property<Guid?>("QuotationId");

                    b.Property<double>("Subtotal");

                    b.HasKey("Id");

                    b.HasIndex("ProductId");

                    b.HasIndex("QuotationId");

                    b.ToTable("ProductDetail");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Provider", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("BusinessName")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<bool>("IsActive");

                    b.Property<string>("PhoneNumber")
                        .IsRequired()
                        .HasMaxLength(15);

                    b.HasKey("Id");

                    b.ToTable("Provider");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Quotation", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("ClientId");

                    b.Property<Guid?>("ClientPetId");

                    b.Property<bool>("IsActive");

                    b.Property<double>("Total");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.HasIndex("ClientPetId");

                    b.ToTable("Quotation");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Quote", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<Guid>("IdClient");

                    b.Property<Guid>("IdPet");

                    b.Property<Guid>("IdUser");

                    b.Property<bool>("IsActive");

                    b.Property<Guid>("ServiceId");

                    b.HasKey("Id");

                    b.HasIndex("ServiceId");

                    b.ToTable("Quote");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Specie", b =>
                {
                    b.Property<string>("Name")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(50);

                    b.Property<string>("Breed")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Name");

                    b.ToTable("Specie");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Gender");

                    b.Property<bool>("IsActive");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("Role");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("User");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Attention", b =>
                {
                    b.HasOne("ServicioVeterinaria.Model.Models.User", "Employee")
                        .WithMany()
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.AttentionDetail", b =>
                {
                    b.HasOne("ServicioVeterinaria.Model.Models.Quotation")
                        .WithMany("ServiceDetail")
                        .HasForeignKey("QuotationId");

                    b.HasOne("ServicioVeterinaria.Model.Models.Attention", "Services")
                        .WithMany()
                        .HasForeignKey("ServicesId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Pet", b =>
                {
                    b.HasOne("ServicioVeterinaria.Model.Models.Client")
                        .WithMany("Pets")
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ServicioVeterinaria.Model.Models.Specie", "Specie")
                        .WithMany()
                        .HasForeignKey("Name")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Product", b =>
                {
                    b.HasOne("ServicioVeterinaria.Model.Models.Provider")
                        .WithMany("Products")
                        .HasForeignKey("ProviderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.ProductDetail", b =>
                {
                    b.HasOne("ServicioVeterinaria.Model.Models.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ServicioVeterinaria.Model.Models.Quotation")
                        .WithMany("ProductDetail")
                        .HasForeignKey("QuotationId");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Quotation", b =>
                {
                    b.HasOne("ServicioVeterinaria.Model.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId");

                    b.HasOne("ServicioVeterinaria.Model.Models.Pet", "ClientPet")
                        .WithMany()
                        .HasForeignKey("ClientPetId");
                });

            modelBuilder.Entity("ServicioVeterinaria.Model.Models.Quote", b =>
                {
                    b.HasOne("ServicioVeterinaria.Model.Models.Attention", "Service")
                        .WithMany()
                        .HasForeignKey("ServiceId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
