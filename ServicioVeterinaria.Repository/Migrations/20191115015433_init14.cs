﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class init14 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attention_User_EmployeeId",
                table: "Attention");

            migrationBuilder.DropIndex(
                name: "IX_Attention_EmployeeId",
                table: "Attention");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Attention_EmployeeId",
                table: "Attention",
                column: "EmployeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attention_User_EmployeeId",
                table: "Attention",
                column: "EmployeeId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
