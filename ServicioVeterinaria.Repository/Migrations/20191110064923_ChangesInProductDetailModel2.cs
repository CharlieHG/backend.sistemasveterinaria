﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class ChangesInProductDetailModel2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Quotation_ProductDetail_SaleId",
                table: "Quotation");

            migrationBuilder.DropForeignKey(
                name: "FK_Quotation_ServiceDetail_ServiceDetailId",
                table: "Quotation");

            migrationBuilder.DropIndex(
                name: "IX_Quotation_SaleId",
                table: "Quotation");

            migrationBuilder.DropIndex(
                name: "IX_Quotation_ServiceDetailId",
                table: "Quotation");

            migrationBuilder.DropColumn(
                name: "SaleId",
                table: "Quotation");

            migrationBuilder.DropColumn(
                name: "ServiceDetailId",
                table: "Quotation");

            migrationBuilder.AddColumn<Guid>(
                name: "QuotationId",
                table: "ServiceDetail",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "QuotationId",
                table: "ProductDetail",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceDetail_QuotationId",
                table: "ServiceDetail",
                column: "QuotationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductDetail_QuotationId",
                table: "ProductDetail",
                column: "QuotationId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductDetail_Quotation_QuotationId",
                table: "ProductDetail",
                column: "QuotationId",
                principalTable: "Quotation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceDetail_Quotation_QuotationId",
                table: "ServiceDetail",
                column: "QuotationId",
                principalTable: "Quotation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductDetail_Quotation_QuotationId",
                table: "ProductDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceDetail_Quotation_QuotationId",
                table: "ServiceDetail");

            migrationBuilder.DropIndex(
                name: "IX_ServiceDetail_QuotationId",
                table: "ServiceDetail");

            migrationBuilder.DropIndex(
                name: "IX_ProductDetail_QuotationId",
                table: "ProductDetail");

            migrationBuilder.DropColumn(
                name: "QuotationId",
                table: "ServiceDetail");

            migrationBuilder.DropColumn(
                name: "QuotationId",
                table: "ProductDetail");

            migrationBuilder.AddColumn<Guid>(
                name: "SaleId",
                table: "Quotation",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ServiceDetailId",
                table: "Quotation",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_SaleId",
                table: "Quotation",
                column: "SaleId");

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_ServiceDetailId",
                table: "Quotation",
                column: "ServiceDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_Quotation_ProductDetail_SaleId",
                table: "Quotation",
                column: "SaleId",
                principalTable: "ProductDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Quotation_ServiceDetail_ServiceDetailId",
                table: "Quotation",
                column: "ServiceDetailId",
                principalTable: "ServiceDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
