﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class pet2_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pet_Specie_Name",
                table: "Pet");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Specie",
                table: "Specie");

            migrationBuilder.DropIndex(
                name: "IX_Pet_Name",
                table: "Pet");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "Specie",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "SpecieId",
                table: "Pet",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Specie",
                table: "Specie",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Pet_SpecieId",
                table: "Pet",
                column: "SpecieId");

            migrationBuilder.AddForeignKey(
                name: "FK_Pet_Specie_SpecieId",
                table: "Pet",
                column: "SpecieId",
                principalTable: "Specie",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pet_Specie_SpecieId",
                table: "Pet");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Specie",
                table: "Specie");

            migrationBuilder.DropIndex(
                name: "IX_Pet_SpecieId",
                table: "Pet");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Specie");

            migrationBuilder.DropColumn(
                name: "SpecieId",
                table: "Pet");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Specie",
                table: "Specie",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Pet_Name",
                table: "Pet",
                column: "Name");

            migrationBuilder.AddForeignKey(
                name: "FK_Pet_Specie_Name",
                table: "Pet",
                column: "Name",
                principalTable: "Specie",
                principalColumn: "Name",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
