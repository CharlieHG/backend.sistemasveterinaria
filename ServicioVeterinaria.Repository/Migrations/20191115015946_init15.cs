﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class init15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttentionDetail_Attention_AttentionId",
                table: "AttentionDetail");

            migrationBuilder.DropIndex(
                name: "IX_AttentionDetail_AttentionId",
                table: "AttentionDetail");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_AttentionDetail_AttentionId",
                table: "AttentionDetail",
                column: "AttentionId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttentionDetail_Attention_AttentionId",
                table: "AttentionDetail",
                column: "AttentionId",
                principalTable: "Attention",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
