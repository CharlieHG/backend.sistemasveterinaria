﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class pet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pet_Client_ClientId",
                table: "Pet");

            migrationBuilder.AlterColumn<Guid>(
                name: "ClientId",
                table: "Pet",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Pet_Client_ClientId",
                table: "Pet",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pet_Client_ClientId",
                table: "Pet");

            migrationBuilder.AlterColumn<Guid>(
                name: "ClientId",
                table: "Pet",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_Pet_Client_ClientId",
                table: "Pet",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
