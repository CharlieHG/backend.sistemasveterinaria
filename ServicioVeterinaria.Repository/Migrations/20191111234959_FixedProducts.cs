﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class FixedProducts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_Provider_ProviderId",
                table: "Product");

            migrationBuilder.DropForeignKey(
                name: "FK_Quote_Service_ServiceId",
                table: "Quote");

            migrationBuilder.DropForeignKey(
                name: "FK_Service_User_EmployeeId",
                table: "Service");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceDetail_Quotation_QuotationId",
                table: "ServiceDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceDetail_Service_ServicesId",
                table: "ServiceDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ServiceDetail",
                table: "ServiceDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Service",
                table: "Service");

            migrationBuilder.RenameTable(
                name: "ServiceDetail",
                newName: "AttentionDetail");

            migrationBuilder.RenameTable(
                name: "Service",
                newName: "Attention");

            migrationBuilder.RenameIndex(
                name: "IX_ServiceDetail_ServicesId",
                table: "AttentionDetail",
                newName: "IX_AttentionDetail_ServicesId");

            migrationBuilder.RenameIndex(
                name: "IX_ServiceDetail_QuotationId",
                table: "AttentionDetail",
                newName: "IX_AttentionDetail_QuotationId");

            migrationBuilder.RenameIndex(
                name: "IX_Service_EmployeeId",
                table: "Attention",
                newName: "IX_Attention_EmployeeId");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProviderId",
                table: "Product",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddPrimaryKey(
                name: "PK_AttentionDetail",
                table: "AttentionDetail",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Attention",
                table: "Attention",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Attention_User_EmployeeId",
                table: "Attention",
                column: "EmployeeId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AttentionDetail_Quotation_QuotationId",
                table: "AttentionDetail",
                column: "QuotationId",
                principalTable: "Quotation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttentionDetail_Attention_ServicesId",
                table: "AttentionDetail",
                column: "ServicesId",
                principalTable: "Attention",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Provider_ProviderId",
                table: "Product",
                column: "ProviderId",
                principalTable: "Provider",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Quote_Attention_ServiceId",
                table: "Quote",
                column: "ServiceId",
                principalTable: "Attention",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attention_User_EmployeeId",
                table: "Attention");

            migrationBuilder.DropForeignKey(
                name: "FK_AttentionDetail_Quotation_QuotationId",
                table: "AttentionDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_AttentionDetail_Attention_ServicesId",
                table: "AttentionDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_Product_Provider_ProviderId",
                table: "Product");

            migrationBuilder.DropForeignKey(
                name: "FK_Quote_Attention_ServiceId",
                table: "Quote");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AttentionDetail",
                table: "AttentionDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Attention",
                table: "Attention");

            migrationBuilder.RenameTable(
                name: "AttentionDetail",
                newName: "ServiceDetail");

            migrationBuilder.RenameTable(
                name: "Attention",
                newName: "Service");

            migrationBuilder.RenameIndex(
                name: "IX_AttentionDetail_ServicesId",
                table: "ServiceDetail",
                newName: "IX_ServiceDetail_ServicesId");

            migrationBuilder.RenameIndex(
                name: "IX_AttentionDetail_QuotationId",
                table: "ServiceDetail",
                newName: "IX_ServiceDetail_QuotationId");

            migrationBuilder.RenameIndex(
                name: "IX_Attention_EmployeeId",
                table: "Service",
                newName: "IX_Service_EmployeeId");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProviderId",
                table: "Product",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ServiceDetail",
                table: "ServiceDetail",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Service",
                table: "Service",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Provider_ProviderId",
                table: "Product",
                column: "ProviderId",
                principalTable: "Provider",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Quote_Service_ServiceId",
                table: "Quote",
                column: "ServiceId",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Service_User_EmployeeId",
                table: "Service",
                column: "EmployeeId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceDetail_Quotation_QuotationId",
                table: "ServiceDetail",
                column: "QuotationId",
                principalTable: "Quotation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceDetail_Service_ServicesId",
                table: "ServiceDetail",
                column: "ServicesId",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
