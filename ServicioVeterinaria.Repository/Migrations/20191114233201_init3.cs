﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class init3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductDetail_Quotation_QuotationId",
                table: "ProductDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_Quotation_Client_ClientId",
                table: "Quotation");

            migrationBuilder.DropForeignKey(
                name: "FK_Quotation_Pet_ClientPetId",
                table: "Quotation");

            migrationBuilder.DropIndex(
                name: "IX_Quotation_ClientId",
                table: "Quotation");

            migrationBuilder.DropIndex(
                name: "IX_Quotation_ClientPetId",
                table: "Quotation");

            migrationBuilder.DropColumn(
                name: "ClientPetId",
                table: "Quotation");

            migrationBuilder.AlterColumn<Guid>(
                name: "ClientId",
                table: "Quotation",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PetId",
                table: "Quotation",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<Guid>(
                name: "QuotationId",
                table: "ProductDetail",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductDetail_Quotation_QuotationId",
                table: "ProductDetail",
                column: "QuotationId",
                principalTable: "Quotation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductDetail_Quotation_QuotationId",
                table: "ProductDetail");

            migrationBuilder.DropColumn(
                name: "PetId",
                table: "Quotation");

            migrationBuilder.AlterColumn<Guid>(
                name: "ClientId",
                table: "Quotation",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "ClientPetId",
                table: "Quotation",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "QuotationId",
                table: "ProductDetail",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_ClientId",
                table: "Quotation",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_ClientPetId",
                table: "Quotation",
                column: "ClientPetId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductDetail_Quotation_QuotationId",
                table: "ProductDetail",
                column: "QuotationId",
                principalTable: "Quotation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Quotation_Client_ClientId",
                table: "Quotation",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Quotation_Pet_ClientPetId",
                table: "Quotation",
                column: "ClientPetId",
                principalTable: "Pet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
