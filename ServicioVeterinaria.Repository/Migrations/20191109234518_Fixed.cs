﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class Fixed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Service_ServiceDetail_ServiceDetailId",
                table: "Service");

            migrationBuilder.DropIndex(
                name: "IX_Service_ServiceDetailId",
                table: "Service");

            migrationBuilder.DropColumn(
                name: "ServiceDetailId",
                table: "Service");

            migrationBuilder.AddColumn<Guid>(
                name: "ServicesId",
                table: "ServiceDetail",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_ServiceDetail_ServicesId",
                table: "ServiceDetail",
                column: "ServicesId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceDetail_Service_ServicesId",
                table: "ServiceDetail",
                column: "ServicesId",
                principalTable: "Service",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceDetail_Service_ServicesId",
                table: "ServiceDetail");

            migrationBuilder.DropIndex(
                name: "IX_ServiceDetail_ServicesId",
                table: "ServiceDetail");

            migrationBuilder.DropColumn(
                name: "ServicesId",
                table: "ServiceDetail");

            migrationBuilder.AddColumn<Guid>(
                name: "ServiceDetailId",
                table: "Service",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Service_ServiceDetailId",
                table: "Service",
                column: "ServiceDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_Service_ServiceDetail_ServiceDetailId",
                table: "Service",
                column: "ServiceDetailId",
                principalTable: "ServiceDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
