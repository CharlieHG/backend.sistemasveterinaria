﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class init6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttentionDetail_Attention_AttentionId",
                table: "AttentionDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_AttentionDetail_Quotation_QuotationId",
                table: "AttentionDetail");

            migrationBuilder.DropIndex(
                name: "IX_AttentionDetail_AttentionId",
                table: "AttentionDetail");

            migrationBuilder.DropColumn(
                name: "IdQuotation",
                table: "AttentionDetail");

            migrationBuilder.AlterColumn<Guid>(
                name: "QuotationId",
                table: "AttentionDetail",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AttentionDetail_Quotation_QuotationId",
                table: "AttentionDetail",
                column: "QuotationId",
                principalTable: "Quotation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttentionDetail_Quotation_QuotationId",
                table: "AttentionDetail");

            migrationBuilder.AlterColumn<Guid>(
                name: "QuotationId",
                table: "AttentionDetail",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "IdQuotation",
                table: "AttentionDetail",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_AttentionDetail_AttentionId",
                table: "AttentionDetail",
                column: "AttentionId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttentionDetail_Attention_AttentionId",
                table: "AttentionDetail",
                column: "AttentionId",
                principalTable: "Attention",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AttentionDetail_Quotation_QuotationId",
                table: "AttentionDetail",
                column: "QuotationId",
                principalTable: "Quotation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
