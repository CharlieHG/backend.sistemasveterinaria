﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioVeterinaria.Repository.Migrations
{
    public partial class init5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttentionDetail_Attention_ServicesId",
                table: "AttentionDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductDetail_Product_ProductId",
                table: "ProductDetail");

            migrationBuilder.DropIndex(
                name: "IX_ProductDetail_ProductId",
                table: "ProductDetail");

            migrationBuilder.RenameColumn(
                name: "ServicesId",
                table: "AttentionDetail",
                newName: "AttentionId");

            migrationBuilder.RenameIndex(
                name: "IX_AttentionDetail_ServicesId",
                table: "AttentionDetail",
                newName: "IX_AttentionDetail_AttentionId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttentionDetail_Attention_AttentionId",
                table: "AttentionDetail",
                column: "AttentionId",
                principalTable: "Attention",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttentionDetail_Attention_AttentionId",
                table: "AttentionDetail");

            migrationBuilder.RenameColumn(
                name: "AttentionId",
                table: "AttentionDetail",
                newName: "ServicesId");

            migrationBuilder.RenameIndex(
                name: "IX_AttentionDetail_AttentionId",
                table: "AttentionDetail",
                newName: "IX_AttentionDetail_ServicesId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductDetail_ProductId",
                table: "ProductDetail",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttentionDetail_Attention_ServicesId",
                table: "AttentionDetail",
                column: "ServicesId",
                principalTable: "Attention",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductDetail_Product_ProductId",
                table: "ProductDetail",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
