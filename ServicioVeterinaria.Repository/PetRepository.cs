﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServicioVeterinaria.Repository
{
    public interface IPetRepository
    {
        IEnumerable<Pet> GetPets();
        Pet GetPet(Guid id);
        Pet Add(Pet Pet);
        bool Update(Guid id, Pet newPet);
        bool Delete(Guid id);
    }
    public class PetRepository : IPetRepository
    {
        private readonly DataDbContext _context = new DataDbContext();
        public Pet Add(Pet Pet)
        {
            _context.Add(Pet);
            _context.SaveChanges();
            return Pet;
        }

        public bool Delete(Guid id)
        {
            var pet = GetPet(id);
            pet.IsActive = !pet.IsActive;
            _context.Pet.Update(pet);
            _context.SaveChanges();
            return true;
        }

        public Pet GetPet(Guid id)
        {
            return _context.Pet.Where(p => p.Id == id).FirstOrDefault();
        }

        public IEnumerable<Pet> GetPets()
        {
            return _context.Set<Pet>();
        }

        public bool Update(Guid id, Pet newPet)
        {
            var pet = GetPet(id);
            pet.BirthDate = newPet.BirthDate;
            pet.ClientId = newPet.ClientId;
            pet.Gender = newPet.Gender;
            pet.Name = newPet.Name;
            pet.Weight = newPet.Weight;
            _context.Pet.Update(pet);
            _context.SaveChanges();
            return true;
        }
    }
}
