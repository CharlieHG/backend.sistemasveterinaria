﻿using ServicioVeterinaria.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioVeterinaria.Repository
{
    public interface IAttentionDetailRepository
    {
        IEnumerable<AttentionDetail> GetAttentionDetails();
        AttentionDetail GetAttentionDetail(int id);
        AttentionDetail Add(AttentionDetail attentionDetail);
        bool Update(int id, AttentionDetail attentionDetail);
        bool Delete(int id);
    }
    public class AttentionDetailRepository : IAttentionDetailRepository
    {
        public AttentionDetail Add(AttentionDetail attentionDetail)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public AttentionDetail GetAttentionDetail(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AttentionDetail> GetAttentionDetails()
        {
            throw new NotImplementedException();
        }

        public bool Update(int id, AttentionDetail attentionDetail)
        {
            throw new NotImplementedException();
        }
    }
}
