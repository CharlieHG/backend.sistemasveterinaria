﻿using ServicioVeterinaria.Model.Models;
using ServicioVeterinaria.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServicioVeterinaria.Repository
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetProducts();
        Product GetProduct(Guid id);
        Product Add(Product Product);
        bool Update(Guid id, Product Product);
        bool Delete(Guid id);
    }
    public class ProductRepository : IProductRepository
    {
        private readonly DataDbContext _context = new DataDbContext();

        public Product Add(Product Product)
        {
            _context.Add(Product);
            _context.SaveChanges();
            return Product;
        }

        public bool Delete(Guid id)
        {
            var product = GetProduct(id);
            product.IsActive = !product.IsActive;
            _context.Product.Update(product);
            _context.SaveChanges();
            return true;
        }

        public IEnumerable<Product> GetProducts()
        {        
                return _context.Set<Product>();  
        }
        public Product GetProduct(Guid id)
        {
            return _context.Product.Where(p => p.Id == id).FirstOrDefault();
        }

        public bool Update(Guid id, Product Product)
        {
            var product = GetProduct(id);
            product.Name = Product.Name;
            product.Price = Product.Price;
            product.Stock = Product.Stock;
            _context.Product.Update(product);
            _context.SaveChanges();
            return true;
        }
    }
}
